﻿using System;

namespace StackOverflow
{
    public class Post
    {
        private string _title;
        private string _description;
        private readonly DateTime _creationDate= DateTime.Now;
        private int _rating;

        

        public void SetTitle(string title)
        {
            _title = title;
        }

        public void SetDescription(string description)
        {
            
            _description = description;
        }

        public void UpVote()
        {
            _rating += 1;
        }

        public void DownVote()
        {
            _rating -= 1;
        }

        public int GetRating()
        {
            return _rating;
        }

        public void DisplayPost()
        {
            Console.Clear();
            Console.WriteLine($"Created:{_creationDate}               Title:{_title}");
            Console.WriteLine("--------------------------------------------------------");
            Console.WriteLine($"{_description}");
            Console.WriteLine("--------------------------------------------------------");
            Console.WriteLine($"Rating:{_rating}");
            Console.WriteLine("_________");
        }
        


    }
}