﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_2
{
    

    class Program
    {
        static void Main(string[] args)
        {
            var trackWatch = new StopWatch();
            Console.WriteLine("00.00.00");
            Console.WriteLine("Type: start or stop or quit");
            
            while (true)
            {
                var response = Console.ReadLine();

                if (response.ToLower() == "start")
                {
                    trackWatch.Start();
                }
                else if (response.ToLower() == "stop")
                {
                    trackWatch.Stop();
                    Console.WriteLine($"total time: {trackWatch.GetDuration()}");
                }
                else if (response.ToLower() == "quit")
                {
                    break;
                }

            }


        }
    }
}
