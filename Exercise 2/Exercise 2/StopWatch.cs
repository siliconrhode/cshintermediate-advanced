﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_2
{
    public class StopWatch
    {
        private DateTime _startTime;
        private DateTime _stopTime;
        private bool _stopWatchStarted;

        public void Start()
        {
            if (_stopWatchStarted)
            {
                throw new InvalidOperationException("Error the watch is already started");
            }
            
            
            _startTime = DateTime.Now;
            _stopWatchStarted = true;

            
            

        }

        public void Stop()
        {
            _stopTime = DateTime.Now;
            
            _stopWatchStarted = false;
            

        }

        public TimeSpan GetDuration()
        {
            if (!_stopWatchStarted)
            {
                return _stopTime - _startTime;
            }
            
            
            return DateTime.Now - _startTime;
            
            
        }
    }
}
