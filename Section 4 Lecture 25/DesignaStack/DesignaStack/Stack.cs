﻿using System;
using System.Collections.Generic;

namespace DesignaStack
{
    public class Stack
    {
        private List<Object> _stackList = new List<object>();

        public void Push(Object obj)
        {
            if(obj == null)
                throw new InvalidOperationException("Please enter a valid value");

            _stackList.Add(obj);

        }

        public object Pop()
        {
            if (_stackList.Count == 0)
                throw new InvalidOperationException();
            var lastItem = _stackList[Math.Max(0, _stackList.Count - 1)];
            int remove = Math.Max(0, _stackList.Count-1);
            _stackList.RemoveAt(remove);
            return lastItem;
        }

        public void Clear()
        {
            if(_stackList.Count == 0)
                throw new InvalidOperationException();
            _stackList = new List<object>();
        }

    }
}