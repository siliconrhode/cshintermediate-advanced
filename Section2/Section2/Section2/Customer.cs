﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Section2
{
    class Customer
    {
        public int Id;
        public string Name;
        public List<Order> Orders;

        public Customer()
        {
            // remember to intialize the list of objects
            Orders = new List<Order>();
        }
        public Customer(int id)
            // it will initialize the list  
            : this()
        {
            this.Id = id;
        }

        public Customer(int id, string name)
            : this(id)
        {
            this.Name = name;
        }
    }
}
