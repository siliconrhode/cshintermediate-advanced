﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grades.Tests
{
    [TestClass]
    public class GradeBookTests
    {
        [TestMethod]

        public void ComputesHighestGrade()
        {
            GradeBook book = new GradeBook(); 
            book.AddGrade(85);
            book.AddGrade(90);
            book.AddGrade(95);

            GradeStatistics statistics = book.ComputeStatistics();
            Assert.AreEqual(95, statistics.HighestGrade);
            
        }

        [TestMethod]
        public void ComputesLowestGrade()
        {
            GradeBook book = new GradeBook();
            book.AddGrade(85);
            book.AddGrade(90);
            book.AddGrade(95);

            GradeStatistics result = book.ComputeStatistics();
            Assert.AreEqual(85,result.LowestGrade);
        }

        [TestMethod]
        public void ComputesAverageGrade()
        {
            GradeBook book = new GradeBook();
            book.AddGrade(91);
            book.AddGrade(89.5f);
            book.AddGrade(75);

            GradeStatistics result = book.ComputeStatistics();
            Assert.AreEqual(85.16,result.AverageGrade,0.01);
        }

    }
}
