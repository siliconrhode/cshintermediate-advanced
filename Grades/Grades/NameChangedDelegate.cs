﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grades
{
    // convention that events follow pass two parameters the first parameter 
    // is the sender of the event, the second parameter is all the needed info about the event
    // build a custom class to put together the new name and the existing name in a single object
    public delegate void NameChangedDelegate(object sender, NameChangedEventArgs args);


}
