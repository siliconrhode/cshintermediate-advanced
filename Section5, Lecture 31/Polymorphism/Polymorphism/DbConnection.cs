﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    public abstract class DbConnection

    {
        public string ConnectionString { get; set; }
        public TimeSpan Timeout { get; set; } 
        public bool ConnectionOn { get; set; } 

        public DbConnection(string stringvalue)
        {
            if(String.IsNullOrEmpty(stringvalue))
                throw new InvalidExpressionException();

            ConnectionString = stringvalue;

            Timeout =new TimeSpan(0, 0, 4);
            
        }

        public abstract void OpenConnection();
        public abstract void CloseConnection();
    }
}
