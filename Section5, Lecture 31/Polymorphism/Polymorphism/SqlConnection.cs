﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    public  class SqlConnection : DbConnection
    {
        public SqlConnection(string stringvalue)
            :base(stringvalue)
        {

            
        }

        public override void OpenConnection()
        {
            var startTime = DateTime.Now;

            if (startTime - DateTime.Now > Timeout)
                throw new InvalidOperationException("Connection time out");

            if (ConnectionOn)
                throw new InvalidOperationException("Error:A connection is already established");

            Console.WriteLine("SQL connection established");
            ConnectionOn = true;

        }

        public override void CloseConnection()
        {
            if(!ConnectionOn)
                throw new InvalidOperationException("Error:No Open connections");
            Console.WriteLine("Closing connection");
        }
    }
}
