﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    
    class Program
    {
        public static void Main(string[] args)
        {
            var newOracleConnection = new OracleConnection("tokennumber2");
            newOracleConnection.OpenConnection();
            var dbinstructions = new DbCommand(newOracleConnection, "runthestuff");
            dbinstructions.Execute();
            newOracleConnection.CloseConnection();



            Console.ReadLine();
        }
        
    }

    
}
