﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    public class DbCommand
    {
        public string Command { get; private set; }

        public DbCommand(DbConnection connection, string command)
        {
            if(connection == null || String.IsNullOrEmpty(command))
                throw new ArgumentNullException();
            if(connection.ConnectionOn == false)
                throw new InvalidOperationException("Error connection isn't open");

            Command = command;
           
        }

        public void Execute()
        {
            Console.WriteLine("Running the instructions"); 
        }

    }
}
