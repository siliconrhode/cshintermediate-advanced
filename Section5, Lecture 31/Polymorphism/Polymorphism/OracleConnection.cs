﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    class OracleConnection : DbConnection
    {
        public OracleConnection(string stringvalue)
            :base(stringvalue)
        {
            
        }
        public override void OpenConnection()
        {
            var startTime = DateTime.Now;

            if (ConnectionOn)
                throw new InvalidOperationException("A connection is already established");
            
            if(startTime - DateTime.Now > Timeout)
                throw new InvalidOperationException("Connection timeout");

            Console.WriteLine("connection established");
            ConnectionOn = true;
        }

        public override void CloseConnection()
        {
            if(!ConnectionOn)
                throw new InvalidOperationException("Error: No open connection");
            ConnectionOn = false;
            Console.WriteLine("Connection terminated");
        }
    }
}
